#[Que-up](www.que-up.surge.sh)


## deployment 

Mobile web app for exposing your spotify queue to your friends.

Host authorizes with the spotify api through the [que server](https://gitlab.com/bmgaynor/que) hosted on heroku.  This auth method enables a longer lasting token and refresh tokens.

Guests authorize with the spotify api with [implict grant](https://developer.spotify.com/web-api/authorization-guide/#implicit_grant_flow)
## Development
```
$ npm install
$ npm start 
```

running backend 

``` 
$ git clone git@gitlab.com:bmgaynor/que.git
$ npm install 
$ npm run dev 

```

## Deployment

Que-up uses gitlab cli and free static hosting with surge.sh.
The backend of Que-up is deployed on heroku.


## Stack 
* Create React App 
* Surge.sh
* Spotify Api
