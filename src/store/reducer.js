import { combineReducers } from 'redux'

const queInitialState = {
  host: {},
  trackList: []
}

const que = (state = queInitialState, {type, payload}) => {
  switch (type) {
    case 'GET_HOST_SUCCESS':
      return Object.assign({}, state, {
        host: payload
      })
    case 'GET_TRACK_LIST_SUCCESS':
      return Object.assign({}, state, {
        trackList: payload
      })
    default:
      return state
  }
}

const spotifyInitialState = {
  searchTerm: '',
  tracks: [],
  guest: {}
}

const spotify = (state = spotifyInitialState, {type, payload}) => {
  switch (type) {
    case 'SEARCH_SONG':
      return Object.assign({}, state, {
        searchTerm: payload
      })
    case 'SEARCH_SONG_SUCCESS':
      return Object.assign({}, state, {
        tracks: payload.tracks
      })
    case 'GET_GUEST_SUCCESS':
      return Object.assign({}, state, {
        guest: payload
      })
    default:
      return state
  }
}

export default combineReducers({
  spotify,
  que
})
