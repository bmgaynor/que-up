import React, { Component } from 'react'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import Next from 'material-ui/svg-icons/image/navigate-next'
import { connect } from 'react-redux'
import { skipSong as skipSongAction, getTrackList as getTrackListAction } from '../store/actions'
import TrackList from '../Components/TrackList'
const style = {
  margin: 20
}

// todo: add in other controls => vote

class Playback extends Component {
  componentWillMount () {
    this.props.getTrackList()
  }
  render () {
    return (
      <div>
        <FloatingActionButton style={style} onClick={this.props.skipSong}>
          <Next />
        </FloatingActionButton>
        <FloatingActionButton style={style} onClick={this.props.skipSong}>
          <Next />
        </FloatingActionButton>
        <FloatingActionButton style={style} onClick={this.props.skipSong}>
          <Next />
        </FloatingActionButton>
        <TrackList songs={this.props.songs} />
      </div>
    )
  }
}

export default connect(
  (state) => ({
    songs: state.que.trackList
  }),
  (dispatch) => ({
    skipSong: () => dispatch(skipSongAction()),
    getTrackList: () => dispatch(getTrackListAction())
  })
)(Playback)
