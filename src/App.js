import React, { Component } from 'react'
import {
  HashRouter as Router,
  Route
} from 'react-router-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import './App.css'
import Header from './Components/Header'
import Search from './Pages/Search'
import Home from './Pages/Home'
import Playback from './Pages/Playback'
import { Provider } from 'react-redux'
import Login from './Pages/Login'
import Host from './Pages/Host'
import reducer from './store/reducer'
import thunk from 'redux-thunk'
import { createStore, applyMiddleware, compose } from 'redux'
import spotifyApi from './utils/spotify'

// import { searchSong } from './store/actions'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
  applyMiddleware(thunk.withExtraArgument(spotifyApi))
))

// spotifyApi.api && store.dispatch(searchSong('phoebe ryan'))

class App extends Component {
  render () {
    return (
      <Provider store={store}>
        <Router>
          <MuiThemeProvider>
            <div className='App'>
              <Header />
              {/* <Router path='/:page' component={Header} /> */}
              <Route path='/search' component={Search} />
              <Route path='/playback' component={Playback} />
              <Route path='/login' component={Login} />
              <Route path='/host' component={Host} />
              <Route path='/home' component={Home} />
              <Route exact path='/' component={Home} />
            </div>
          </MuiThemeProvider>
        </Router>
      </Provider>
    )
  }
}

export default App
