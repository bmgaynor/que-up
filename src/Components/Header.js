import React from 'react'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'
import AppBar from 'material-ui/AppBar'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {open: false};
  }

  handleToggle = () => this.setState({open: !this.state.open})

  handleClose = () => this.setState({open: false})

  render() {
    return (
      <div>
        <AppBar
          title="Que Up"
          iconClassNameRight="muidocs-icon-navigation-expand-more"
          onTitleTouchTap={this.handleToggle}
          onLeftIconButtonTouchTap={this.handleToggle}
        />
        <Drawer
          docked={false}
          width={200}
          open={this.state.open}
          onRequestChange={(open) => this.setState({open})}
        >
          <MenuItem
            containerElement={<Link to="/" />}
            onClick={this.handleClose}>Home</MenuItem>
          {!this.props.guest.display_name && <MenuItem
            containerElement={<Link to="/login" />}
            onClick={this.handleClose}>Login</MenuItem>}
          <MenuItem
            containerElement={<Link to="/search" />}
            onClick={this.handleClose}>Queue songs</MenuItem>
          <MenuItem
            containerElement={<Link to="/playback" />}
            onClick={this.handleClose}>Playback Controls</MenuItem>
          <MenuItem
            containerElement={<Link to="/host" />}
            onClick={this.handleClose}>Host It</MenuItem>
        </Drawer>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    guest: state.spotify.guest
  })
)(Header)
