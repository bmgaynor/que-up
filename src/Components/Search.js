import React from 'react'
import { connect } from 'react-redux'
import TextField from 'material-ui/TextField'
import { searchSong } from '../store/actions'

const Search = ({searchTerm, search}) => {
  return (
    <div>
      <TextField
        id='text-field-controlled'
        value={searchTerm}
        onChange={(event) => search(event.target.value)}
        hintText='Search Song'
      />
    </div>
  )
}

export default connect(
  (state) => ({
    searchTerm: state.spotify.searchTerm
  }),
  (dispatch) => ({
    search: (string) => dispatch(searchSong(string))
  }))(Search)
